# RPGCharacters
RPG Characters is a console appilcation game where you can currently choose between four classes/heroes: Mage, Ranger, Rogue, Warrior.

Characters in the game have several types of attributes, which represent different aspects of the character. They are represented as Primary attributes.

Primary attributes are what determine the characters power, as the character levels up their primary attributes increase. Items they equip also can increase these primary attributes.

## Attributes
The attribute system found in this assignment is based on the traditional Three-Stat System leaning towards a Diablo 3 implementation. Firstly, looking at primary attributes:
* Strength – determines the physical strength of the character.
    - Each point of strength increases a Warriors damage by 1%.
* Dexterity – determines the characters ability to attack with speed and nimbleness.
    - Each point of dexterity increases a Rangers and Rogues damage by 1%.
* Intelligence – determines the characters affinity with magic.
    - Each point of intelligence increases a Mages damage by 1%.

Finally, a character can deal Damage. The amount of damage depends on the weapon equipped, the character class, and the amount of primary attribute the character has. 

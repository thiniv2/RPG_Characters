﻿using rpgCharacters.Characters;
using rpgCharacters.Exceptions;
using rpgCharacters;

namespace rpgCharactersTests
{
    public class ItemTests
    {
        [Fact]
        public void EquipWeapon_CheckIfRangerLevelIsEnoughToEquipTheWeapon_ShouldThrowInvalidWeaponException()
        {
            //Arrange
            Ranger ranger = new Ranger();
            Weapons testBow = new Weapons()
            {
                Name = "Common bow",
                LevelRequirement = 2,
                ItemSlot = Slot.Weapon,
                WeaponTypes = Weapons.CharacterWeaponTypes.Bow,
                WeaponAttributes = new WeaponAttributes() { Damage = 2, AttackSpeed = 1.5 }
            };
            string expected = "You can not equip this weapon.";

            //Act
            InvalidWeaponException actual = Assert.Throws<InvalidWeaponException>(() => ranger.EquipWeapon(testBow));

            //Assert
            Assert.Equal(expected, actual.Message);
        }

        [Fact]
        public void EquipArmor_CheckIfRangerLevelIsEnoughtoEquipTheArmor_ShouldThrowInvalidArmorException()
        {
            //Arrange
            Ranger ranger = new Ranger();
            Armors testLeatherArmor = new Armors()
            {
                Name = "Common Leather armor",
                LevelRequirement = 2,
                ItemSlot = Slot.Body,
                ArmorTypes = Armors.CharacterArmorTypes.Leather,
                Attributes = new PrimaryAttributes() { Dexterity = 1 }
            };
            string expected = "You can not equip this armor.";

            //Act
            InvalidArmorException actual = Assert.Throws<InvalidArmorException>(() => ranger.EquipArmor(testLeatherArmor));

            //Assert
            Assert.Equal(expected, actual.Message);
        }

        [Fact]
        public void EquipWeapon_CheckIfWarriorCanUseBow_ShouldThrowInvalidWeaponException()
        {
            //Arrange
            Warrior warrior = new Warrior();
            Weapons testBow = new Weapons()
            {
                Name = "Common bow",
                LevelRequirement = 1,
                ItemSlot = Slot.Weapon,
                WeaponTypes = Weapons.CharacterWeaponTypes.Bow,
                WeaponAttributes = new WeaponAttributes() { Damage = 6, AttackSpeed = 1.2 }
            };
            string expected = "You can not equip this weapon.";

            //Act
            InvalidWeaponException actual = Assert.Throws<InvalidWeaponException>(() => warrior.EquipWeapon(testBow));

            //Assert
            Assert.Equal(expected, actual.Message);
        }

        [Fact]
        public void EquipWeapon_CheckIfRangerCanUseClothArmor_ShouldThrowInvalidArmorException()
        {
            //Arrange
            Ranger ranger = new Ranger();
            Armors testClothArmor = new Armors()
            {
                Name = "Common Cloth Armor",
                LevelRequirement = 1,
                ItemSlot = Slot.Body,
                ArmorTypes = Armors.CharacterArmorTypes.Cloth,
                Attributes = new PrimaryAttributes() { Intelligence = 3 }
            };
            string expected = "You can not equip this armor.";

            //Act
            InvalidArmorException actual = Assert.Throws<InvalidArmorException>(() => ranger.EquipArmor(testClothArmor));

            //Assert
            Assert.Equal(expected, actual.Message);
        }

        [Fact]
        public void EquipArmor_CheckIfRangerCanWearLeatherArmor_ShouldReturnSuccessfulMessage()
        {
            //Arrange
            Ranger ranger = new Ranger();
            Armors testLeatherHelmet = new Armors()
            {
                Name = "Common Leather Helmet",
                LevelRequirement = 1,
                ItemSlot = Slot.Head,
                ArmorTypes = Armors.CharacterArmorTypes.Leather,
                Attributes = new PrimaryAttributes() { Dexterity = 6 }
            };
            string expected = "Equipped new armor!";

            //Act
            string actual = ranger.EquipArmor(testLeatherHelmet);

            //Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void EquipWeapon_EquipAValidWeaponForWarriorCharacter_ShouldReturnASuccessMessage()
        {
            //Arrange
            Warrior warrior = new Warrior();
            Weapons testAxe = new Weapons()
            {
                Name = "Common axe",
                LevelRequirement = 1,
                ItemSlot = Slot.Weapon,
                WeaponTypes = Weapons.CharacterWeaponTypes.Axe,
                WeaponAttributes = new WeaponAttributes() { Damage = 7, AttackSpeed = 1.1 }
            };
            string expected = "Equipped new weapon!";

            //Act
            string actual = warrior.EquipWeapon(testAxe);

            //Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void EquipArmor_EquipAValidArmorForWarriorCharacter_ShouldReturnASuccessMessage()
        {
            //Arrange
            Warrior warrior = new Warrior();
            Armors testPlateBody = new Armors()
            {
                Name = "Common plate body armor",
                LevelRequirement = 1,
                ItemSlot = Slot.Body,
                ArmorTypes = Armors.CharacterArmorTypes.Plate,
                Attributes = new PrimaryAttributes() { Strength = 1 }
            };
            string expected = "Equipped new armor!";

            //Act
            string actual = warrior.EquipArmor(testPlateBody);

            //Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void Damage_CalculateTheDamageOfWarriorCharacterIfNoWeaponIsEquipped_ShouldReturnOneAndFiveHundredths()
        {
            //Arrange
            Warrior warrior = new Warrior();
            double expected = 1 * (1 + (5 * 0.01));

            //Act
            double actual = warrior.Damage();

            //Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void Damage_CalculateTheDamageOfWarriorCharacterWithAxeWeaponEquipped_ShouldReturnEightAndEightyFiveThousand()
        {
            //Arrange
            Warrior warrior = new Warrior();
            Weapons testAxe = new Weapons()
            {
                Name = "Common axe",
                LevelRequirement = 1,
                ItemSlot = Slot.Weapon,
                WeaponTypes = Weapons.CharacterWeaponTypes.Axe,
                WeaponAttributes = new WeaponAttributes() { Damage = 7, AttackSpeed = 1.1 }
            };
            warrior.EquipWeapon(testAxe);
            double expected = (7 * 1.1) * (1 + (5 * 0.01));

            //Act
            double actual = warrior.Damage();

            //Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void Damage_CalculateTheDamageOfWarriorCharacterWithAxeWeaponAndPlateBodyArmorEquipped_ShouldReturnEightPointOneHundredSixtyTwo()
        {
            //Arranges
            Warrior warrior = new Warrior();
            Weapons testAxe = new Weapons()
            {
                Name = "Common axe",
                LevelRequirement = 1,
                ItemSlot = Slot.Weapon,
                WeaponTypes = Weapons.CharacterWeaponTypes.Axe,
                WeaponAttributes = new WeaponAttributes() { Damage = 7, AttackSpeed = 1.1 }
            };
            Armors testPlateBody = new Armors()
            {
                Name = "Common plate body armor",
                LevelRequirement = 1,
                ItemSlot = Slot.Body,
                ArmorTypes = Armors.
                CharacterArmorTypes.Plate,
                Attributes = new PrimaryAttributes() { Strength = 1 }
            };
            warrior.EquipWeapon(testAxe);
            warrior.EquipArmor(testPlateBody);
            double expected = (7 * 1.1) * (1 + ((5 + 1) * 0.01));

            //Act
            double actual = warrior.Damage();

            //Assert
            Assert.Equal(expected, actual);
        }
    }
}

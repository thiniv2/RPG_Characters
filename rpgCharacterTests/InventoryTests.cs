using rpgCharacters.Exceptions;
using rpgCharacters.Inventory;

namespace InventoryManagerTests
{
    public class InventoryTests
    {
        #region Creation
        [Fact]
        public void Constructor_InitializeWithSize_ShouldCreateInventoryWithSize()
        {
            // Arrange
            int size = 5;
            int expected = size;
            // Act
            Inventory inventory = new Inventory(size);
            int actual = inventory.GetInventorySize();
            // Assert
            Assert.Equal(expected, actual);
        }

        [Theory]
        [InlineData(0)]
        [InlineData(-1)]
        public void Constructor_InvalidSize_ShouldThrowInventoryException(int size)
        {
            Assert.Throws<InventoryException>(() => new Inventory(size));
        }
        #endregion

        #region Inserting items
        [Fact]
        public void AddItem_AddSingleItem_ShouldAddToFirstSlot()
        {
            // Arrange
            int size = 1;
            Inventory inventory = new Inventory(size);
            Item itemToAdd = new Item() { Name = "Test Item", LevelRequirement = 10 };
            int expected = 1;
            // Act
            int actual = inventory.AddItem(itemToAdd);
            // Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void AddItem_AddTwoItem_ShouldReturnSlot()
        {
            // Arrange
            int size = 2;
            Inventory inventory = new Inventory(size);
            Item itemToAddOne = new Item() { Name = "Test Item", LevelRequirement = 10 };
            Item itemToaddTwo = itemToAddOne;
            int expected = 2;
            inventory.AddItem(itemToAddOne);
            // Act
            int actual = inventory.AddItem(itemToaddTwo);
            // Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void AddItem_AddItemToFullInventory_ShouldThrowInventoryException()
        {
            // Arrange
            int size = 1;
            Inventory inventory = new Inventory(size);
            Item itemToAddOne = new Item() { Name = "Test Item", LevelRequirement = 10 };
            Item itemToaddTwo = itemToAddOne;
            inventory.AddItem(itemToAddOne);
            // Act and Assert
            Assert.Throws<InventoryException>(() => inventory.AddItem(itemToaddTwo));
        }

        #endregion

        #region Removing items
        [Fact]
        public void RemoveItem_RemoveValidItemInSlot_RemoveTheItem()
        {
            // Arrange
            int size = 1;
            Inventory inventory = new Inventory(size);
            Item itemToAdd = new Item() { Name = "Test item", LevelRequirement = 10 };
            inventory.AddItem(itemToAdd);
            int slot = 1;
            Item expected = itemToAdd;
            // Act
            Item actual = inventory.RemoveItem(slot);
            // Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void RemoveItem_RemoveEmptySlot_ShouldThrowInventoryException()
        {
            // Arrange
            int size = 1;
            Inventory inventory = new Inventory(size);
            int slot = 1;
            // Act and Assert
            Assert.Throws<InventoryException>(() => inventory.RemoveItem(slot));
        }

        [Theory]
        [InlineData(0)]
        [InlineData(-1)]
        [InlineData(2)]
        public void RemoveItem_InvalidSlot_ShouldThrowInventoryException(int slot)
        {
            // Arrange
            int size = 1;
            Inventory inventory = new Inventory(size);
            Item itemToAdd = new Item() { Name = "Test item", LevelRequirement = 10 };
            inventory.AddItem(itemToAdd);
            // Act and Assert
            Assert.Throws<InventoryException>(() => inventory.RemoveItem(slot));
        }
        #endregion
    }
}
﻿namespace rpgCharacters
{
    public enum Slot
    {
        Head,
        Body,
        Legs,
        Weapon
    }
}

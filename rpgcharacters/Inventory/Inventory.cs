﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using rpgCharacters.Exceptions;

namespace rpgCharacters.Inventory
{
    public class Inventory
    {
        private Dictionary<int, Item> ItemsInventory { get; set; }

        /// <summary>
        /// Creates Inventory with size of 'size', sets each index value null
        /// </summary>
        /// <param name="size">Size of the inventory</param>
        /// <exception cref="InventoryException">Size is less than 1</exception>
        public Inventory(int size)
        {
            if (size < 1)
                throw new InventoryException("Can't initialize an inventory with a size less than 1 slots.");
            InitializeInventory(size);
        }

        private void InitializeInventory(int size)
        {
            ItemsInventory = new Dictionary<int, Item>();
            for (int i = 1; i <= size; i++)
            {
                ItemsInventory.Add(i, null);
            }
        }

        /// <summary>
        /// Displays the size of the current inventory
        /// </summary>
        /// <returns>The inventory size</returns>
        public int GetInventorySize()
        {
            return ItemsInventory.Count;
        }

        /// <summary>
        /// Adds a new item to the inventory in the first available slot
        /// </summary>
        /// <param name="itemToAdd">Item that is added to the inventory</param>
        /// <returns>The slot the item was added into</returns>
        /// <exception cref="InventoryException">When the inventory is full</exception>
        public int AddItem(Item itemToAdd)
        {
            // Loop through all the slots
            foreach (KeyValuePair<int, Item> item in ItemsInventory)
            {
                // Checks if there is an item in the slot
                if (item.Value == null)
                {
                    // If there isnt, we add the item to that slot
                    ItemsInventory[item.Key] = itemToAdd;
                    return item.Key;
                }
            }
            // If we get here, we know there are no slots
            throw new InventoryException("Inventory is full");
        }

        /// <summary>
        /// Removes item from inventory if slot is valid
        /// </summary>
        /// <param name="slot"></param>
        /// <returns>Removed item</returns>
        /// <exception cref="InventoryException">Slot is invalid</exception>
        public Item RemoveItem(int slot)
        {
            if (slot < 1 || slot > ItemsInventory.Count)
                throw new InventoryException("Must select a valid slot");

            if (ItemsInventory[slot] == null)
                throw new InventoryException("No item in that slot");

            Item removedItem = ItemsInventory[slot];
            ItemsInventory[slot] = null;
            return removedItem;
        }
    }
}
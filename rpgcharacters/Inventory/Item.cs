﻿namespace rpgCharacters.Inventory
{
    public class Item
    {
        public string? Name { get; set; }
        public Slot ItemSlot { get; set; }
        public int LevelRequirement { get; set; }
    }
}

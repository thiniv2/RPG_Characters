﻿using rpgCharacters.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace rpgCharacters.Characters
{
    public class Mage : Character
    {
        public Mage()
        {
            Class = "Mage";
            // Set primary attribute values
            PrimaryAttributes = new PrimaryAttributes
            {
                Strength = 1,
                Dexterity = 1,
                Intelligence = 8,
            };
        }

        // Increases Rangers primary attributes
        public override void IncreaseLevel()
        {
            PrimaryAttributes.Strength += 1;
            PrimaryAttributes.Dexterity += 1;
            PrimaryAttributes.Intelligence += 5;
            Level += 1;
        }

        // Checks if weapon is equippable by our character
        // and adds it to the equipment dictionary
        public override string EquipWeapon(Weapons weapon)
        {
            if (weapon.WeaponTypes == Weapons.CharacterWeaponTypes.Staff
                && weapon.ItemSlot == Slot.Weapon && Level >= weapon.LevelRequirement
                || weapon.WeaponTypes == Weapons.CharacterWeaponTypes.Wand
                && weapon.ItemSlot == Slot.Weapon && Level >= weapon.LevelRequirement)
            {
                Equipment.Add(weapon.ItemSlot, weapon);
                return "Equipped new weapon!";
            }
            else
            {
                throw new InvalidWeaponException();
            }
        }

        // Checks if armor is equippable by our character
        // and adds it to the equipment dictionary
        public override string EquipArmor(Armors armor)
        {
            if (armor.ArmorTypes == Armors.CharacterArmorTypes.Cloth
                && armor.ItemSlot != Slot.Weapon
                && Level >= armor.LevelRequirement)
            {
                Equipment.Add(armor.ItemSlot, armor);
                return "Equipped new armor!";
            }
            else
            {
                throw new InvalidArmorException();
            }
        }

        public override double TotalAttributes()
        {
            TotalPrimaryAttributes = 0;
            foreach (var item in Equipment)
            {
                if (item.Key != Slot.Weapon)
                {
                    Armors gainedArmor = (Armors)Equipment[item.Key];
                    TotalPrimaryAttributes += gainedArmor.Attributes.Intelligence;
                }
            }
            TotalPrimaryAttributes += PrimaryAttributes.Intelligence;
            return TotalPrimaryAttributes;
        }
    }
}

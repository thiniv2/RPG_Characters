﻿using rpgCharacters.Exceptions;

namespace rpgCharacters.Characters
{
    public class Ranger : Character
    {
        public Ranger()
        {
            Class = "Ranger";
            // Set primary attribute values
            PrimaryAttributes = new PrimaryAttributes
            {
                Strength = 1,
                Dexterity = 7,
                Intelligence = 1,
            };
        }

        // Increases Rangers primary attributes
        public override void IncreaseLevel()
        {
            PrimaryAttributes.Strength += 1;
            PrimaryAttributes.Dexterity += 5;
            PrimaryAttributes.Intelligence += 1;
            Level += 1;
        }

        // Checks if weapon is equippable by our character
        // and adds it to the equipment dictionary
        public override string EquipWeapon(Weapons weapon)
        {
            if (weapon.WeaponTypes == Weapons.CharacterWeaponTypes.Bow
                && weapon.ItemSlot == Slot.Weapon && Level >= weapon.LevelRequirement)
            {
                Equipment.Add(weapon.ItemSlot, weapon);
                return "Equipped new weapon!";
            }
            else
            {
                throw new InvalidWeaponException();
            }
        }

        // Checks if armor is equippable by our character
        // and adds it to the equipment dictionary
        public override string EquipArmor(Armors armor)
        {
            if (armor.ArmorTypes == Armors.CharacterArmorTypes.Mail
                && armor.ItemSlot != Slot.Weapon
                && Level >= armor.LevelRequirement
                || armor.ArmorTypes == Armors.CharacterArmorTypes.Leather
                && armor.ItemSlot != Slot.Weapon
                && Level >= armor.LevelRequirement)
            {
                Equipment.Add(armor.ItemSlot, armor);
                return "Equipped new armor!";
            }
            else
            {
                throw new InvalidArmorException();
            }
        }

        public override double TotalAttributes()
        {
            TotalPrimaryAttributes = 0;
            foreach (var item in Equipment)
            {
                if (item.Key != Slot.Weapon)
                {
                    Armors gainedArmor = (Armors)Equipment[item.Key];
                    TotalPrimaryAttributes += gainedArmor.Attributes.Dexterity;
                }
            }
            TotalPrimaryAttributes += PrimaryAttributes.Dexterity;
            return TotalPrimaryAttributes;
        }
    }
}

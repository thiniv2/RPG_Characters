﻿using rpgCharacters.Inventory;
namespace rpgCharacters
{
    public class Weapons : Item
    {
        public WeaponAttributes? WeaponAttributes { get; set; }
        public CharacterWeaponTypes WeaponTypes { get; set; }

        public enum CharacterWeaponTypes
        {
            Axe,
            Bow,
            Dagger,
            Hammer,
            Staff,
            Sword,
            Wand
        }

        public double CalculateDps()
        {
            return WeaponAttributes.Damage * WeaponAttributes.AttackSpeed;
        }
    }

}

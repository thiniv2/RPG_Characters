﻿using rpgCharacters.Inventory;
using System.Text;

namespace rpgCharacters
{
    public abstract class Character
    {
        public string? Name { get; set; }
        public string Class { get; set; }
        public int Level { get; set; } = 1;
        public PrimaryAttributes PrimaryAttributes { get; set; }
        public double TotalPrimaryAttributes { get; set; }

        public Dictionary<Slot, Item> Equipment = new Dictionary<Slot, Item>();

        public abstract void IncreaseLevel();
        public abstract string EquipWeapon(Weapons weapon);
        public abstract string EquipArmor(Armors armor);
        public abstract double TotalAttributes();

        public virtual double Damage()
        {
            double damage = 0;

            if (Equipment.ContainsKey(Slot.Weapon))
            {
                Weapons weapon = (Weapons)Equipment[Slot.Weapon];
                damage += weapon.CalculateDps() * (1 + (TotalAttributes() / 100));
                return Math.Round(damage, 3);
            }
            damage += 1 * (1 + (TotalAttributes() / 100));
            return Math.Round(damage, 3);
        }

        public string displayStats()
        {
            foreach (var item in Equipment)
            {
                if (item.Key != Slot.Weapon)
                {
                    Armors gainedArmor = (Armors)Equipment[item.Key];
                    PrimaryAttributes.Strength += gainedArmor.Attributes.Strength;
                    PrimaryAttributes.Dexterity += gainedArmor.Attributes.Dexterity;
                    PrimaryAttributes.Intelligence += gainedArmor.Attributes.Intelligence;

                }
            }
            StringBuilder sb = new StringBuilder();
            sb.Append($"Name: {Name}\n");
            sb.Append($"Level: {Level}\n");
            sb.Append($"Strength: {PrimaryAttributes.Strength}\n");
            sb.Append($"Dexterity: {PrimaryAttributes.Dexterity}\n");
            sb.Append($"Intelligence: {PrimaryAttributes.Intelligence}\n");
            sb.Append($"Damage: {Damage()}\n");
            return sb.ToString();
        }
    }
}

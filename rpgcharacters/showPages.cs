﻿using System.Reflection.Emit;

namespace rpgCharacters
{
    public class showPages
    {
        public void ShowMenu(Character player)
        {
            if (player.Name == null)
            {
                Console.WriteLine("Enter your name:");
                player.Name = Console.ReadLine();
            }

            Console.WriteLine(@"
Select action:
(1) Profile Page
(2) Weapon Page
(3) Armor Page
(esc) Exit program");

            var menuInput = Console.ReadKey();
            if (menuInput.Key == ConsoleKey.D1)
            {
                Console.Clear();
                ProfilePage(player);
            }
            else if (menuInput.Key == ConsoleKey.D2)
            {
                Console.Clear();
                weaponPage(player);
            }
            else if (menuInput.Key == ConsoleKey.D3)
            {
                Console.Clear();
                armorPage(player);
            }
            else if (menuInput.Key == ConsoleKey.Escape)
            {
                System.Environment.Exit(0);
            }
            else
            {
                Console.Clear();
                Console.WriteLine("\nInvalid input\n");
                ShowMenu(player);
            }
        }

        public void ProfilePage(Character player)
        {
            Console.WriteLine(player.displayStats());
            Console.WriteLine(@"
Choose action: 
(A) Increase level

(1) Menu Page
(2) Weapon Page
(3) Armor Page
(esc) Exit program");
            var profileInput = Console.ReadKey();
            if (profileInput.Key == ConsoleKey.D1)
            {
                Console.Clear();
                ShowMenu(player);
            }
            else if (profileInput.Key == ConsoleKey.A)
            {
                player.IncreaseLevel();
                Console.Clear();
                ProfilePage(player);
            }
            else if (profileInput.Key == ConsoleKey.D2)
            {
                Console.Clear();
                weaponPage(player);
            }
            else if (profileInput.Key == ConsoleKey.D3)
            {
                Console.Clear();
                armorPage(player);
            }
            else if (profileInput.Key == ConsoleKey.Escape)
            {
                System.Environment.Exit(0);
            }
            else
            {
                Console.Clear();
                Console.WriteLine("\nInvalid input\n");
                ProfilePage(player);
            }
        }

        public void weaponPage(Character player)
        {
            Console.WriteLine(@"
Choose your weapon:
(q) common Axe (Level Requirement: 2)
(w) *Bow
(e) *Dagger
(r) *Hammer
(a) *Staff
(s) *Sword
(d) *Wand

(*) Not yet implemented

(1) Menu page
(2) Profile page");

            var weaponInput = Console.ReadKey();

            if (weaponInput.Key == ConsoleKey.Q)
            {
                Weapons commonAxe = new Weapons()
                {
                    Name = "Common Axe",
                    LevelRequirement = 2,
                    ItemSlot = Slot.Weapon,
                    WeaponTypes = Weapons.CharacterWeaponTypes.Axe,
                    WeaponAttributes = new WeaponAttributes() { Damage = 2, AttackSpeed = 1.5 }
                };
                if (player.Level >= 2
                    && player.Class == "Warrior"
                    && !player.Equipment.ContainsKey(commonAxe.ItemSlot))
                {
                    Console.Clear();
                    Console.WriteLine(player.EquipWeapon(commonAxe));
                    weaponPage(player);
                }
                else
                {
                    Console.Clear();
                    Console.WriteLine("Requirements not met");
                    weaponPage(player);
                }
            }

            else if (weaponInput.Key == ConsoleKey.D1)
            {
                Console.Clear();
                ShowMenu(player);
            }
            else if (weaponInput.Key == ConsoleKey.D2)
            {
                Console.Clear();
                ProfilePage(player);
            }
            else if (weaponInput.Key == ConsoleKey.Escape)
            {
                System.Environment.Exit(0);
            }
            else
            {
                Console.Clear();
                Console.WriteLine("\nInvalid input\n");
                weaponPage(player);
            }
        }

        public void armorPage(Character player)
        {
            Console.WriteLine(@"
Choose your armor:
(q) *Cloth
(w) *Leather
(e) *Mail
(r) Rune Platebody

(*) Not yet implemented

(1) Menu page
(2) Profile page");

            var armorInput = Console.ReadKey();

            if (armorInput.Key == ConsoleKey.R)
            {
                Armors runePlateBody = new Armors()
                {
                    Name = "Rune Platebody",
                    LevelRequirement = 1,
                    ItemSlot = Slot.Body,
                    ArmorTypes = Armors.CharacterArmorTypes.Plate,
                    Attributes = new PrimaryAttributes() { Strength = 6 }
                };
                if (player.Level >= 1
                    && player.Class == "Warrior"
                    && !player.Equipment.ContainsKey(runePlateBody.ItemSlot))
                {
                    Console.Clear();
                    Console.WriteLine(player.EquipArmor(runePlateBody));
                    armorPage(player);
                }
                else
                {
                    Console.Clear();
                    Console.WriteLine("Requirements not met");
                    armorPage(player);
                }
            }

            else if (armorInput.Key == ConsoleKey.D1)
            {
                Console.Clear();
                ShowMenu(player);
            }
            else if (armorInput.Key == ConsoleKey.D2)
            {
                Console.Clear();
                ProfilePage(player);
            }
            else if (armorInput.Key == ConsoleKey.Escape)
            {
                System.Environment.Exit(0);
            }
            else
            {
                Console.Clear();
                Console.WriteLine("\nInvalid input\n");
                weaponPage(player);
            }
        }
    }
}

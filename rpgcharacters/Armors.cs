﻿using rpgCharacters.Inventory;

namespace rpgCharacters
{
    public class Armors : Item
    {
        public PrimaryAttributes Attributes { get; set; }
        public CharacterArmorTypes ArmorTypes { get; set; }

        public enum CharacterArmorTypes
        {
            Cloth,
            Leather,
            Mail,
            Plate
        }
    }
}

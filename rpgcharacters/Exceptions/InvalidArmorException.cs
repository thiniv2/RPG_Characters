﻿namespace rpgCharacters.Exceptions
{
    public class InvalidArmorException : Exception
    {
        public override string Message => "You can not equip this armor.";
    }
}

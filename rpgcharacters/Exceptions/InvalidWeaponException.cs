﻿namespace rpgCharacters.Exceptions
{
    public class InvalidWeaponException : Exception
    {
        public override string Message => "You can not equip this weapon.";
    }
}

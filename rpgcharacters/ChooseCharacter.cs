﻿using rpgCharacters.Characters;

namespace rpgCharacters
{
    public class ChooseCharacterClass
    {
        public void chooseCharacter()
        {
            Console.WriteLine(@"
Choose your character:
(1) Mage
(2) Rogue
(3) Warrior
(4) Ranger
(esc) Exit program");

            var charSelectInput = Console.ReadKey();
            if (charSelectInput.Key == ConsoleKey.D1)
            {
                Mage mage = new Mage();
                var ShowMenu = new showPages();
                Console.Clear();
                ShowMenu.ShowMenu(mage);
            }
            else if (charSelectInput.Key == ConsoleKey.D2)
            {
                Rogue rogue = new Rogue();
                var ShowMenu = new showPages();
                Console.Clear();
                ShowMenu.ShowMenu(rogue);
            }
            else if (charSelectInput.Key == ConsoleKey.D3)
            {
                Warrior warrior = new Warrior();
                var ShowMenu = new showPages();
                Console.Clear();
                ShowMenu.ShowMenu(warrior);
            }
            else if (charSelectInput.Key == ConsoleKey.D4)
            {
                Ranger ranger = new Ranger();
                var ShowMenu = new showPages();
                Console.Clear();
                ShowMenu.ShowMenu(ranger);
            }
            else if (charSelectInput.Key == ConsoleKey.Escape)
            {
                System.Environment.Exit(0);
            }
            else
            {
                Console.Clear();
                Console.WriteLine("\nInvalid input");
                chooseCharacter();
            }
        }
    }
}
